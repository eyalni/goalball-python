from matplotlib import pyplot as plt
import cv2
import numpy as np

def find_homography(linebuilder):
    court_regtangle_points = ()
    # Four corners of the book in source image
    court_theory_pts = np.array([[0, 0], [200, 0], [200, 100], [0, 100]])

    # Take the first four points
    court_image_pts = np.array([[linebuilder.xs[i],linebuilder.ys[i]] for i in range(4)])

    homography_trans, status = cv2.findHomography(court_image_pts, court_theory_pts)
    return homography_trans, status



class LineBuilder:
    def __init__(self, line):
        self.line = line
        # self.xs = list(line.get_xdata())
        # self.ys = list(line.get_ydata())
        self.xs = list()
        self.ys = list()

        self.cid = line.figure.canvas.mpl_connect('button_press_event', self)

    def __call__(self, event):
        print('click', event)
        print('Button : %d' % (event.button))

        # Right click - remove last point
        if (event.button == 3):
            if (len(self.xs) > 0):
                last_ind_to_remove = len(self.xs) - 1
                self.xs.pop(last_ind_to_remove)
                self.ys.pop(last_ind_to_remove)

                cur_data = self.line.get_data()
                new_data = (cur_data[0][0:-1] ,cur_data[0][0:-1])
                self.line.get_data(new_data)

        if (event.dblclick==True):
            print ('This is double click - end the plot')
            plt.close()

        if event.inaxes!=self.line.axes: return
        self.xs.append(event.xdata)
        self.ys.append(event.ydata)
        self.line.set_data(self.xs, self.ys)
        self.line.figure.canvas.draw()

fig = plt.figure()
ax = fig.add_subplot(111)
ax.set_title('click to build line segments')
line, = ax.plot([0], [0])  # empty line
linebuilder = LineBuilder(line)

plt.show()

homography_trans, homgraphy_status = find_homography(linebuilder)

print ('End of program')